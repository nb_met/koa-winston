# koa-winston

## Usage

### Installation

```shell
npm i --save koa-winston
```

### Quick Start

```ts
import { logger } from 'koa-winston/dist';
app.use(logger());
```


