/* eslint no-param-reassign: 0 */
import * as Koa from 'koa'
import * as winston from 'winston'
import * as onFinished from 'on-finished'
import { stringify, KoaLogSchema } from './Schema'

export interface payload {
    transports?: any[];
    level?: string;
    logger?: winston.Logger;
}

export const logger = (payload: payload = {}) => {
    const {
        transports = [new winston.transports.Stream({ stream: process.stdout })]
    } = payload;
    
    const winstonLogger = payload.logger
        || winston.createLogger({
            transports,
            format: winston.format.combine(
                winston.format.json(),
                winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss.SSS' }),
                winston.format.printf((info) => `${info.timestamp} [${info.level.toUpperCase()}] - ${info.message}`)
            ),
        });

    const onResponseFinished = (ctx: Koa.ExtendableContext, startAt: [number, number]) => {
        let endAt = process.hrtime();
        const messageSchema: KoaLogSchema = {
            req: { host: ctx.hostname, ip: ctx.ip, url: ctx.url, query: ctx.query, body: ctx.request['body'] },
            res: { body: ctx.body, status: `${ctx.status}`, ms: ((endAt[0] - startAt[0]) * 1e3 + (endAt[1] - startAt[1]) * 1e-6).toFixed(3) },
        };

        const info = {
            level: getLogLevel(ctx.status),
            message: stringify(messageSchema)
        }

        winstonLogger.log(info);
    };

    return async (ctx: Koa.ExtendableContext, next: any) => {
        let startAt: [Number, Number] = process.hrtime();

        let error: any;
        try {
            await next();
        } catch (e) {
            error = e;
        } finally {
            onFinished(ctx.response.res, onResponseFinished.bind(null, ctx, startAt));
        }

        if (error) {
            throw error;
        }
    };
};

export enum eLogLevel {
    debug = 'debug',
    info = 'info',
    warn = 'warn',
    error = 'error'
}

export function getLogLevel(statusCode = 200, defaultLevel = eLogLevel.info) {
    switch (Math.floor(statusCode / 100)) {
        case 5: return eLogLevel.error;
        case 4: return eLogLevel.warn;
        default: return defaultLevel;
    }
};
