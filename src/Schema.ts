import * as fastJson from 'fast-json-stringify'
import * as winston from 'winston'

export const stringify = fastJson({
  type: 'object',
  properties: {
    req: {
      type: 'object',
      properties: {
        host: { type: 'string' },
        ip: { type: 'string' },
        url: { type: 'string' },
        query: {
          type: 'object',
          additionalProperties: { type: 'string' },
        },
        body: {
          type: 'object',
          additionalProperties: { type: 'string' },
        },
      },
    },
    res: {
      type: 'object',
      properties: {
        ms: { type: 'string' },
        status: { type: 'string' },
        body: {
          type: 'object',
          additionalProperties: true,
        },
      },
    },
  },
});

export interface KoaReqSchema {
  host: string
  ip: string
  url: string
  query: object
  body: object
}

export interface KoaResSchema {
  ms: string
  status: string
  body: object
}

export interface KoaLogSchema {
  req: KoaReqSchema
  res: KoaResSchema
}
