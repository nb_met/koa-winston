"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const winston = require("winston");
const onFinished = require("on-finished");
const Schema_1 = require("./Schema");
exports.logger = (payload = {}) => {
    const { transports = [new winston.transports.Stream({ stream: process.stdout })] } = payload;
    const winstonLogger = payload.logger
        || winston.createLogger({
            transports,
            format: winston.format.combine(winston.format.json(), winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss.SSS' }), winston.format.printf((info) => `${info.timestamp} [${info.level.toUpperCase()}] - ${info.message}`)),
        });
    const onResponseFinished = (ctx, startAt) => {
        let endAt = process.hrtime();
        const messageSchema = {
            req: { host: ctx.hostname, ip: ctx.ip, url: ctx.url, query: ctx.query, body: ctx.request['body'] },
            res: { body: ctx.body, status: `${ctx.status}`, ms: ((endAt[0] - startAt[0]) * 1e3 + (endAt[1] - startAt[1]) * 1e-6).toFixed(3) },
        };
        const info = {
            level: getLogLevel(ctx.status),
            message: Schema_1.stringify(messageSchema)
        };
        winstonLogger.log(info);
    };
    return (ctx, next) => __awaiter(this, void 0, void 0, function* () {
        let startAt = process.hrtime();
        let error;
        try {
            yield next();
        }
        catch (e) {
            error = e;
        }
        finally {
            onFinished(ctx.response.res, onResponseFinished.bind(null, ctx, startAt));
        }
        if (error) {
            throw error;
        }
    });
};
var eLogLevel;
(function (eLogLevel) {
    eLogLevel["debug"] = "debug";
    eLogLevel["info"] = "info";
    eLogLevel["warn"] = "warn";
    eLogLevel["error"] = "error";
})(eLogLevel = exports.eLogLevel || (exports.eLogLevel = {}));
function getLogLevel(statusCode = 200, defaultLevel = eLogLevel.info) {
    switch (Math.floor(statusCode / 100)) {
        case 5: return eLogLevel.error;
        case 4: return eLogLevel.warn;
        default: return defaultLevel;
    }
}
exports.getLogLevel = getLogLevel;
;
//# sourceMappingURL=index.js.map