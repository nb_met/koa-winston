import * as Koa from 'koa';
import * as winston from 'winston';
export interface payload {
    transports?: any[];
    level?: string;
    logger?: winston.Logger;
}
export declare const logger: (payload?: payload) => (ctx: Koa.ExtendableContext, next: any) => Promise<void>;
export declare enum eLogLevel {
    debug = "debug",
    info = "info",
    warn = "warn",
    error = "error"
}
export declare function getLogLevel(statusCode?: number, defaultLevel?: eLogLevel): eLogLevel.debug | eLogLevel | eLogLevel.error;
