"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fastJson = require("fast-json-stringify");
// todo 
exports.stringify = fastJson({
    type: 'object',
    properties: {
        req: {
            type: 'object',
            properties: {
                host: { type: 'string' },
                ip: { type: 'string' },
                url: { type: 'string' },
                query: {
                    type: 'object',
                    additionalProperties: { type: 'string' },
                },
            },
        },
        res: {
            type: 'object',
            properties: {
                ms: { type: 'string' },
                status: { type: 'string' },
                body: {
                    type: 'object',
                    additionalProperties: true,
                },
            },
        },
    },
});
//# sourceMappingURL=stringify.js.map