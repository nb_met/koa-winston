export declare const stringify: (doc: object) => string;
export interface KoaReqSchema {
    host: string;
    ip: string;
    url: string;
    query: object;
    body: object;
}
export interface KoaResSchema {
    ms: string;
    status: string;
    body: object;
}
export interface KoaLogSchema {
    req: KoaReqSchema;
    res: KoaResSchema;
}
